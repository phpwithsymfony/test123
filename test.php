<?php

namespace CustomerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JmesPath\Tests\_TestClass;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="customer")
 * @ORM\Entity(repositoryClass="CustomerBundle\Repository\CustomerRepository")
 */
class Customer
{
    const STATUS_RETURNING = 'Active - Returning';
    const STATUS_NEW       = 'Active - New';
    const STATUS_DORMANT   = 'Dormant';
    const STATUS_NO_CUSTOM = 'No Custom';

    const UNVERIFIED       = 'Unverified';
    const NO_ACTIVITY      = 'No Activity';
    const INACTIVE         = 'Inactive';
    const ACTIVE           = 'Active';
    const INACTIVE_PREMIUM = 'Inactive Premium';
    const ACTIVE_PREMIUM   = 'Active Premium';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="payment_firstname", type="string", length=32, nullable=true)
     * @Assert\NotBlank()
     */
    private $paymentFirstname;

    /**
     * @ORM\Column(name="payment_lastname", type="string", length=32, nullable=true)
     */
    private $paymentLastname;

    /**
     * @ORM\Column(name="payment_email", type="string", length=96, nullable=true)
     * @Assert\Email()
     */
    private $paymentEmail = null;

    /**
     * @ORM\Column(name="payment_company", type="string", length=128, nullable=true)
     */
    private $paymentCompany = null;

    /**
     * @ORM\Column(name="payment_social_channel", type="string", length=128, nullable=true)
     */
    private $paymentSocialChannel = null;

    /**
     * @ORM\Column(name="payment_social_channel_name", type="string", length=128, nullable=true)
     */
    private $paymentSocialChannelName = null;

    /**
     * @ORM\ManyToOne(targetEntity="CustomerBundle\Entity\PublishLocation", inversedBy="customer")
     * @ORM\JoinColumn(name="publish_location_id", referencedColumnName="id")
     */
    protected $paymentPublishLocation;

    /**
     * @ORM\Column(name="payment_vat_number", type="string", length=40, nullable=true)
     */
    private $paymentVatNumber = null;

    /**
     * @ORM\Column(name="payment_address1", type="string", length=128, nullable=true)
     */
    private $paymentAddress1 = null;

    /**
     * @ORM\Column(name="payment_address2", type="string", length=128, nullable=true)
     */
    private $paymentAddress2 = null;

    /**
     * @ORM\Column(name="payment_city", type="string", length=128, nullable=true)
     */
    private $paymentCity = null;

    /**
     * @ORM\Column(name="payment_postcode", type="string", length=10, nullable=true)
     */
    private $paymentPostcode = null;

    /**
     * @ORM\Column(name="payment_region", type="string", length=128, nullable=true)
     */
    private $paymentRegion = null;

    /**
     * @ORM\ManyToOne(targetEntity="\LocalisationBundle\Entity\Country", inversedBy="countries")
     * @ORM\JoinColumn(name="payment_country_id", referencedColumnName="id", nullable=true)
     */
    private $paymentCountry = null;

    /**
     * @ORM\Column(name="payment_company_url", type="string", length=200, nullable=true)
     */
    private $paymentCompanyUrl;

    /**
     * @ORM\Column(name="status", type="string", length=128, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(name="active", type="boolean")
     */
    private $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Assert\Date()
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Assert\Date()
     */
    private $updated;

    /**
     * @ORM\OneToOne(targetEntity="\UserBundle\Entity\User", inversedBy="customer")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     * @Assert\NotNull()
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="\OrderBundle\Entity\Order", mappedBy="customer")
     */
    private $orders;

    /**
     * @ORM\ManyToOne(targetEntity="\LocalisationBundle\Entity\Currency", inversedBy="customers")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * @ORM\OneToOne(targetEntity="\OrderBundle\Entity\Basket", mappedBy="customer", cascade={"persist"})
     */
    protected $basket;

    /**
     * Many Customers have Many NewsletterSubscriptions.
     * @ORM\ManyToMany(targetEntity="NewsletterSubscription", inversedBy="customers")
     * @ORM\JoinTable(name="customers_newsletter_subscriptions")
     */
    private $newsletterSubscriptions;

    /**
     * @ORM\OneToMany(targetEntity="CustomerBundle\Entity\Playlist" , mappedBy="customer", cascade={"remove"})
     * */
    protected $playlists;

    /**
     * @ORM\Column(name="is_trusted", type="boolean", options={"default": 0})
     */
    private $isTrusted = 0;

    /**
     * @ORM\Column(name="trusted_name", type="string", length=128, nullable=true)
     */
    private $trustedName = null;

    /**
     * @ORM\OneToMany(targetEntity="CustomerBundle\Entity\CustomerFollowing" , mappedBy="customer")
     * */
    protected $customerFollowings;

    /**
     * Many Customers have Many Tracks.
     * @ORM\ManyToMany(targetEntity="TrackBundle\Entity\Track")
     * @ORM\JoinTable(name="customer_to_favourite",
     *      joinColumns={@ORM\JoinColumn(name="customer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="track_id", referencedColumnName="id")}
     *      )
     */
    protected $favourites;

    /**
     * Many Customers have Many TrackVersions.
     * @ORM\OneToMany(targetEntity="TrackBundle\Entity\CustomerToFavoriteTracksVersion" , mappedBy="customer",
     *     fetch="EXTRA_LAZY", cascade={"remove"})
     * @ORM\OrderBy({"id" = "ASC"})
     */
    protected $favouriteTrackVersions;

    /**
     * @ORM\OneToMany(targetEntity="\OrderBundle\Entity\DiscountCode", mappedBy="singleUseCustomer")
     */
    private $singleUseDiscountCodes;

    /**
     * @ORM\Column(name="is_engagement", type="boolean", options={"default": 0})
     */
    private $isEngagement = 0;

    /**
     * @ORM\OneToMany(targetEntity="TrackBundle\Entity\TrackEngagementPending", mappedBy="customer")
     */
    private $pendingEngagements;

    /**
     * @ORM\Column(name="stripe_id", type="string", length=100, nullable=true, options={"default": NULL})
     */
    private $stripeId;

    /**
     * @ORM\OneToMany(targetEntity="SubscriptionBundle\Entity\CustomerSubPlans", mappedBy="customerId")
     */
    private $subscriptionPlan;

    /**
     * @ORM\OneToMany(targetEntity="SubscriptionBundle\Entity\SubscriptionUsageLog", mappedBy="customerId")
     */
    private $subscriptionUsages;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="MainSiteBundle\Entity\MetaEventLogs", mappedBy="customerId")
     * @ORM\OrderBy({"dateCreated" = "DESC"})
     */
    private $metaEventsLogs;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_activity_date", type="datetime", nullable=true)
     */
    private $lastActivityDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="active_months", type="integer", nullable=true)
     */
    private $activeMonths;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_subscribed_any_time", type="boolean", options={"default": 0})
     */
    private $isSubscribedAnyTime = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_free_trial_eligible", type="boolean", options={"default": true})
     */
    private $isFreeTrialEligible = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreated(new \DateTime());

        if ($this->getUpdated() == null) {
            $this->setUpdated(new \DateTime());
        }

        $this->orders          = new \Doctrine\Common\Collections\ArrayCollection();
        $this->trackToPlaylist = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTimestamps()
    {
        // update the updated time
        $this->setUpdated(new \DateTime());

        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime());
        }
    }

    /**
     * __toString method
     */
    public function __toString()
    {
        return ($this->getUser()) ? $this->getUser()->getEmail() : '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the full name
     */
    public function getName()
    {
        return $this->getPaymentFirstname() . ' ' . $this->getPaymentLastname();
    }

    /**
     * Set paymentFirstname
     *
     * @param string $paymentFirstname
     *
     * @return Customer
     */
    public function setPaymentFirstname($paymentFirstname)
    {
        $this->paymentFirstname = $paymentFirstname;

        return $this;
    }

    /**
     * Get paymentFirstname
     *
     * @return string
     */
    public function getPaymentFirstname()
    {
        return $this->paymentFirstname;
    }

    /**
     * Set paymentLastname
     *
     * @param string $paymentLastname
     *
     * @return Customer
     */
    public function setPaymentLastname($paymentLastname)
    {
        $this->paymentLastname = $paymentLastname;

        return $this;
    }

    /**
     * Get paymentLastname
     *
     * @return string
     */
    public function getPaymentLastname()
    {
        return $this->paymentLastname;
    }

    /**
     * Set paymentEmail
     *
     * @param string $paymentEmail
     *
     * @return Customer
     */
    public function setPaymentEmail($paymentEmail)
    {
        $this->paymentEmail = $paymentEmail;

        return $this;
    }

    /**
     * Get paymentEmail
     *
     * @return string
     */
    public function getPaymentEmail()
    {
        return $this->paymentEmail;
    }

    /**
     * Set paymentCompany
     *
     * @param string $paymentCompany
     *
     * @return Customer
     */
    public function setPaymentCompany($paymentCompany)
    {
        $this->paymentCompany = $paymentCompany;

        return $this;
    }

    /**
     * Get paymentCompany
     *
     * @return string
     */
    public function getPaymentCompany()
    {
        return $this->paymentCompany;
    }

    /**
     * Set paymentSocialChannel
     *
     * @param string $paymentSocialChannel
     *
     * @return Customer
     */
    public function setPaymentSocialChannel($paymentSocialChannel)
    {
        $this->paymentSocialChannel = $paymentSocialChannel;

        return $this;
    }

    /**
     * Get paymentSocialChannel
     *
     * @return string
     */
    public function getPaymentSocialChannel()
    {
        return $this->paymentSocialChannel;
    }

    /**
     * Set paymentSocialChannelName
     *
     * @param string $paymentSocialChannelName
     *
     * @return Customer
     */
    public function setPaymentSocialChannelName($paymentSocialChannelName)
    {
        $this->paymentSocialChannelName = $paymentSocialChannelName;

        return $this;
    }

    /**
     * Get paymentSocialChannelName
     *
     * @return string
     */
    public function getPaymentSocialChannelName()
    {
        return $this->paymentSocialChannelName;
    }

    /**
     * Set paymentPublishLocation
     *
     * @param \CustomerBundle\Entity\PublishLocation $paymentPublishLocation
     *
     * @return PublishLocation
     */
    public function setPaymentPublishLocation(\CustomerBundle\Entity\PublishLocation $paymentPublishLocation = null)
    {
        $this->paymentPublishLocation = $paymentPublishLocation;

        return $this;
    }

    /**
     * Get paymentPublishLocation
     *
     * @return \CustomerBundle\Entity\PublishLocation
     */
    public function getPaymentPublishLocation()
    {
        return $this->paymentPublishLocation;
    }

    /**
     * Set paymentVatNumber
     *
     * @param string $paymentVatNumber
     *
     * @return Customer
     */
    public function setPaymentVatNumber($paymentVatNumber)
    {
        $formattedVatNumber = preg_replace('/\s+/', '', strtoupper($paymentVatNumber));

        $this->paymentVatNumber = $formattedVatNumber;

        return $this;
    }

    /**
     * Get paymentVatNumber
     *
     * @return string
     */
    public function getPaymentVatNumber()
    {
        return $this->paymentVatNumber;
    }

    /**
     * Set paymentAddress1
     *
     * @param string $paymentAddress1
     *
     * @return Customer
     */
    public function setPaymentAddress1($paymentAddress1)
    {
        $this->paymentAddress1 = $paymentAddress1;

        return $this;
    }

    /**
     * Get paymentAddress1
     *
     * @return string
     */
    public function getPaymentAddress1()
    {
        return $this->paymentAddress1;
    }

    /**
     * Set paymentAddress2
     *
     * @param string $paymentAddress2
     *
     * @return Customer
     */
    public function setPaymentAddress2($paymentAddress2)
    {
        $this->paymentAddress2 = $paymentAddress2;

        return $this;
    }

    /**
     * Get paymentAddress2
     *
     * @return string
     */
    public function getPaymentAddress2()
    {
        return $this->paymentAddress2;
    }

    /**
     * Set paymentCity
     *
     * @param string $paymentCity
     *
     * @return Customer
     */
    public function setPaymentCity($paymentCity)
    {
        $this->paymentCity = $paymentCity;

        return $this;
    }

    /**
     * Get paymentCity
     *
     * @return string
     */
    public function getPaymentCity()
    {
        return $this->paymentCity;
    }

    /**
     * Set paymentPostcode
     *
     * @param string $paymentPostcode
     *
     * @return Customer
     */
    public function setPaymentPostcode($paymentPostcode)
    {
        $this->paymentPostcode = $paymentPostcode;

        return $this;
    }

    /**
     * Get paymentPostcode
     *
     * @return string
     */
    public function getPaymentPostcode()
    {
        return $this->paymentPostcode;
    }

    /**
     * Set paymentRegion
     *
     * @param string $paymentRegion
     *
     * @return Customer
     */
    public function setPaymentRegion($paymentRegion)
    {
        $this->paymentRegion = $paymentRegion;

        return $this;
    }

    /**
     * Get paymentRegion
     *
     * @return string
     */
    public function getPaymentRegion()
    {
        return $this->paymentRegion;
    }

    /**
     * Set paymentCountry
     *
     * @param string $paymentCountry
     *
     * @return Customer
     */
    public function setPaymentCountry($paymentCountry)
    {
        $this->paymentCountry = $paymentCountry;

        return $this;
    }

    /**
     * Get paymentCountry
     *
     * @return string
     */
    public function getPaymentCountry()
    {
        return $this->paymentCountry;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Customer
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Customer
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Customer
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Role string to be used in Role\UserRole
     *
     * @return string
     */
    public function getRole()
    {
        return 'ROLE_CUSTOMER';
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Customer
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add order
     *
     * @param \OrderBundle\Entity\Order $order
     *
     * @return Customer
     */
    public function addOrder(\OrderBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \OrderBundle\Entity\Order $order
     */
    public function removeOrder(\OrderBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set basket
     *
     * @param \OrderBundle\Entity\Basket $basket
     *
     * @return Customer
     */
    public function setBasket(\OrderBundle\Entity\Basket $basket = null)
    {
        $this->basket = $basket;

        return $this;
    }

    /**
     * Get basket
     *
     * @return \OrderBundle\Entity\Basket
     */
    public function getBasket()
    {
        return $this->basket;
    }

    /**
     * Add newsletterSubscription
     *
     * @param \CustomerBundle\Entity\NewsletterSubscription $newsletterSubscription
     *
     * @return Customer
     */
    public function addNewsletterSubscription(\CustomerBundle\Entity\NewsletterSubscription $newsletterSubscription)
    {
        $this->newsletterSubscriptions[] = $newsletterSubscription;

        return $this;
    }

    /**
     * Remove newsletterSubscription
     *
     * @param \CustomerBundle\Entity\NewsletterSubscription $newsletterSubscription
     */
    public function removeNewsletterSubscription(\CustomerBundle\Entity\NewsletterSubscription $newsletterSubscription)
    {
        $this->newsletterSubscriptions->removeElement($newsletterSubscription);
    }

    /**
     * Get newsletterSubscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewsletterSubscriptions()
    {
        return $this->newsletterSubscriptions;
    }

    /**
     * Set currency
     *
     * @param \LocalisationBundle\Entity\Currency $currency
     *
     * @return Customer
     */
    public function setCurrency(\LocalisationBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \LocalisationBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Add playlist
     *
     * @param \CustomerBundle\Entity\Playlist $playlist
     *
     * @return Customer
     */
    public function addPlaylist(\CustomerBundle\Entity\Playlist $playlist)
    {
        $this->playlists[] = $playlist;

        return $this;
    }

    /**
     * Remove playlist
     *
     * @param \CustomerBundle\Entity\Playlist $playlist
     */
    public function removePlaylist(\CustomerBundle\Entity\Playlist $playlist)
    {
        $this->playlists->removeElement($playlist);
    }

    /**
     * Get playlists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlaylists()
    {
        return $this->playlists;
    }

    /**
     * Add favourite
     *
     * @param \TrackBundle\Entity\Track $favourite
     *
     * @return Customer
     */
    public function addFavourite(\TrackBundle\Entity\Track $favourite)
    {
        $this->favourites[] = $favourite;

        return $this;
    }

    /**
     * Remove favourite
     *
     * @param \TrackBundle\Entity\Track $favourite
     */
    public function removeFavourite(\TrackBundle\Entity\Track $favourite)
    {
        $this->favourites->removeElement($favourite);
    }

    /**
     * Get favourites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavourites()
    {
        return $this->favourites;
    }

    /**
     * Add customerFollowing
     *
     * @param \CustomerBundle\Entity\CustomerFollowing $customerFollowing
     *
     * @return Customer
     */
    public function addCustomerFollowing(\CustomerBundle\Entity\CustomerFollowing $customerFollowing)
    {
        $this->customerFollowings[] = $customerFollowing;

        return $this;
    }

    /**
     * Remove customerFollowing
     *
     * @param \CustomerBundle\Entity\CustomerFollowing $customerFollowing
     */
    public function removeCustomerFollowing(\CustomerBundle\Entity\CustomerFollowing $customerFollowing)
    {
        $this->customerFollowings->removeElement($customerFollowing);
    }

    /**
     * Get customerFollowings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomerFollowings()
    {
        return $this->customerFollowings;
    }

    /**
     * Add singleUseDiscountCode
     *
     * @param \OrderBundle\Entity\DiscountCode $singleUseDiscountCode
     *
     * @return Customer
     */
    public function addSingleUseDiscountCode(\OrderBundle\Entity\DiscountCode $singleUseDiscountCode)
    {
        $this->singleUseDiscountCodes[] = $singleUseDiscountCode;

        return $this;
    }

    /**
     * Remove singleUseDiscountCode
     *
     * @param \OrderBundle\Entity\DiscountCode $singleUseDiscountCode
     */
    public function removeSingleUseDiscountCode(\OrderBundle\Entity\DiscountCode $singleUseDiscountCode)
    {
        $this->singleUseDiscountCodes->removeElement($singleUseDiscountCode);
    }

    /**
     * Get singleUseDiscountCodes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSingleUseDiscountCodes()
    {
        return $this->singleUseDiscountCodes;
    }


    /**
     * Get the total spend for the Customer
     */
    public function getTotalSpend()
    {
        $totalSpend = 0;

        foreach ($this->getOrders() as $order) {
            if ($order->getOrderFinancialRecord()) {
                $totalSpend += $order->getOrderFinancialRecord()->getSubTotal();
            }
        }

        return $totalSpend;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Customer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getNameForDisplay()
    {
        if (strlen($this->getName()) > 15) {
            return substr($this->getName(), 0, 15) . '...';
        } else {
            return $this->getName();
        }
    }

    public function getPaymentCompanyForDisplay()
    {
        if (strlen($this->getPaymentCompany()) > 15) {
            return substr($this->getPaymentCompany(), 0, 15) . '...';
        } else {
            return $this->getPaymentCompany();
        }
    }

    /**
     * @return boolean
     */
    public function getIsTrusted()
    {
        return $this->isTrusted;
    }

    /**
     * @param boolean $isTrusted
     *
     * @return $this
     */
    public function setIsTrusted($isTrusted)
    {
        $this->isTrusted = $isTrusted;

        return $this;
    }

    /**
     * Set trustedName
     *
     * @param string $trustedName
     *
     * @return Customer
     */
    public function setTrustedName($trustedName)
    {
        $this->trustedName = $trustedName;

        return $this;
    }

    /**
     * Get trustedName
     *
     * @return string
     */
    public function getTrustedName()
    {
        return $this->trustedName;
    }

    /**
     * @return boolean
     */
    public function getIsEngagement()
    {
        return $this->isEngagement;
    }

    /**
     * @param boolean $isEngagement
     *
     * @return $this
     */
    public function setIsEngagement($isEngagement)
    {
        $this->isEngagement = $isEngagement;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPendingEngagements()
    {
        return $this->pendingEngagements;
    }

    /**
     * @param mixed $pendingEngagements
     *
     * @return $this
     */
    public function setPendingEngagements($pendingEngagements)
    {
        $this->pendingEngagements = $pendingEngagements;

        return $this;
    }

    /**
     * @return string
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * @param string $stripeId
     */
    public function setStripeId($stripeId)
    {
        $this->stripeId = $stripeId;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionPlan()
    {
        return $this->subscriptionPlan;
    }

    /**
     * @param $subscriptionPlan
     *
     * @return $this
     */
    public function setSubscriptionPlan($subscriptionPlan)
    {
        $this->subscriptionPlan = $subscriptionPlan;

        return $this;
    }

    /**
     * Set paymentCompanyUrl
     *
     * @param string $paymentCompanyUrl
     *
     * @return Customer
     */
    public function setPaymentCompanyUrl($paymentCompanyUrl)
    {
        $this->paymentCompanyUrl = $paymentCompanyUrl;

        return $this;
    }

    /**
     * Get paymentCompanyUrl
     *
     * @return string
     */
    public function getPaymentCompanyUrl()
    {
        return $this->paymentCompanyUrl;
    }

    /**
     * Add metaEventsLog
     *
     * @param \MainSiteBundle\Entity\MetaEventLogs $metaEventsLog
     *
     * @return Order
     */
    public function addMetaEventsLog(\MainSiteBundle\Entity\MetaEventLogs $metaEventsLog)
    {
        $this->metaEventsLogs[] = $metaEventsLog;

        return $this;
    }

    /**
     * Remove metaEventsLog
     *
     * @param \MainSiteBundle\Entity\MetaEventLogs $metaEventsLog
     */
    public function removeMetaEventsLog(\MainSiteBundle\Entity\MetaEventLogs $metaEventsLog)
    {
        $this->metaEventsLogs->removeElement($metaEventsLog);
    }

    /**
     * Get metaEventsLogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMetaEventsLogs()
    {
        return $this->metaEventsLogs;
    }

    /**
     * Add favouriteTrackVersion
     *
     * @param \TrackBundle\Entity\CustomerToFavoriteTracksVersion $favouriteTrackVersion
     *
     * @return Customer
     */
    public function addFavouriteTrackVersion(\TrackBundle\Entity\CustomerToFavoriteTracksVersion $favouriteTrackVersion)
    {
        $this->favouriteTrackVersions[] = $favouriteTrackVersion;

        return $this;
    }

    /**
     * Remove favouriteTrackVersion
     *
     * @param \TrackBundle\Entity\CustomerToFavoriteTracksVersion $favouriteTrackVersion
     */
    public function removeFavouriteTrackVersion(\TrackBundle\Entity\CustomerToFavoriteTracksVersion $favouriteTrackVersion)
    {
        $this->favouriteTrackVersions->removeElement($favouriteTrackVersion);
    }

    /**
     * Get favouriteTrackVersions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavouriteTrackVersions()
    {
        return $this->favouriteTrackVersions;
    }

    /**
     * Add subscriptionUsage
     *
     * @param \SubscriptionBundle\Entity\SubscriptionUsageLog $subscriptionUsage
     *
     * @return Customer
     */
    public function addSubscriptionUsage(\SubscriptionBundle\Entity\SubscriptionUsageLog $subscriptionUsage)
    {
        $this->subscriptionUsages[] = $subscriptionUsage;

        return $this;
    }

    /**
     * Remove subscriptionUsage
     *
     * @param \SubscriptionBundle\Entity\SubscriptionUsageLog $subscriptionUsage
     */
    public function removeSubscriptionUsage(\SubscriptionBundle\Entity\SubscriptionUsageLog $subscriptionUsage)
    {
        $this->subscriptionUsages->removeElement($subscriptionUsage);
    }

    /**
     * Get subscriptionUsages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscriptionUsages()
    {
        return $this->subscriptionUsages;
    }

    /**
     * Set lastActivityDate
     *
     * @param \DateTime $lastActivityDate
     *
     * @return Customer
     */
    public function setLastActivityDate($lastActivityDate)
    {
        $this->lastActivityDate = $lastActivityDate;

        return $this;
    }

    /**
     * Get lastActivityDate
     *
     * @return \DateTime
     */
    public function getLastActivityDate()
    {
        return $this->lastActivityDate;
    }

    /**
     * Set activeMonths
     *
     * @param integer $activeMonths
     *
     * @return Customer
     */
    public function setActiveMonths($activeMonths)
    {
        $this->activeMonths = $activeMonths;

        return $this;
    }

    /**
     * Get activeMonths
     *
     * @return integer
     */
    public function getActiveMonths()
    {
        return $this->activeMonths;
    }

    /**
     * Set isSubscribedAnyTime
     *
     * @param boolean $isSubscribedAnyTime
     *
     * @return Customer
     */
    public function setIsSubscribedAnyTime($isSubscribedAnyTime)
    {
        $this->isSubscribedAnyTime = $isSubscribedAnyTime;

        return $this;
    }

    /**
     * Get isSubscribedAnyTime
     *
     * @return boolean
     */
    public function getIsSubscribedAnyTime()
    {
        return $this->isSubscribedAnyTime;
    }

    /**
     * Set isFreeTrialEligible
     *
     * @param boolean $isFreeTrialEligible
     *
     * @return Customer
     */
    public function setIsFreeTrialEligible($isFreeTrialEligible)
    {
        $this->isFreeTrialEligible = $isFreeTrialEligible;

        return $this;
    }

    /**
     * Get isFreeTrialEligible
     *
     * @return boolean
     */
    public function getIsFreeTrialEligible()
    {
        return $this->isFreeTrialEligible;
    }

    /**
     * Add pendingEngagement
     *
     * @param \TrackBundle\Entity\TrackEngagementPending $pendingEngagement
     *
     * @return Customer
     */
    public function addPendingEngagement(\TrackBundle\Entity\TrackEngagementPending $pendingEngagement)
    {
        $this->pendingEngagements[] = $pendingEngagement;

        return $this;
    }

    /**
     * Remove pendingEngagement
     *
     * @param \TrackBundle\Entity\TrackEngagementPending $pendingEngagement
     */
    public function removePendingEngagement(\TrackBundle\Entity\TrackEngagementPending $pendingEngagement)
    {
        $this->pendingEngagements->removeElement($pendingEngagement);
    }

    /**
     * Add subscriptionPlan
     *
     * @param \SubscriptionBundle\Entity\CustomerSubPlans $subscriptionPlan
     *
     * @return Customer
     */
    public function addSubscriptionPlan(\SubscriptionBundle\Entity\CustomerSubPlans $subscriptionPlan)
    {
        $this->subscriptionPlan[] = $subscriptionPlan;

        return $this;
    }

    /**
     * Remove subscriptionPlan
     *
     * @param \SubscriptionBundle\Entity\CustomerSubPlans $subscriptionPlan
     */
    public function removeSubscriptionPlan(\SubscriptionBundle\Entity\CustomerSubPlans $subscriptionPlan)
    {
        $this->subscriptionPlan->removeElement($subscriptionPlan);
    }
}

